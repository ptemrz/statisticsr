import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/guesser",
    name: "Guesser",
    component: () =>
      import(/* webpackChunkName: "guesser" */ "../views/Guesser.vue")
  },
  {
    path: "/played",
    name: "Played",
    component: () =>
      import(/* webpackChunkName: "played" */ "../views/Played.vue")
  },
  {
    path: "/schedule",
    name: "Schedule",
    component: () =>
      import(/* webpackChunkName: "schedule" */ "../views/Schedule.vue")
  },
  {
    path: "/toplist",
    name: "Toplist",
    component: () =>
      import(/* webpackChunkName: "toplist" */ "../views/Toplist.vue")
  },

  {
    path: "/channels",
    name: "Channels",

    component: () => import("../views/Channels.vue")
  },
  {
    path: "/error",
    name: "Error",

    component: () => import("../views/Error.vue")
  }
];

const router = new VueRouter({
  scrollBehavior: function() {
    return { x: 0, y: 0 };
  },
  routes
});

export default router;

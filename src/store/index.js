import Vue from "vue";
import Vuex from "vuex";
import statisticsr from "@/services/statisticsr.js";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    topWords: [],
    allSongs: [],
    test: "Hej",
    colors: ["#fde23e", "#f16e23", "#57d9ff", "#973e88", "#01142F"]
  },

  mutations: {
    addWords(state, data) {
      state.topWords.push(data);
    },
    clearWords(state) {
      state.topWords = [];
    }
  },

  actions: {
    updateWords(state, year) {
      statisticsr
        .getTopTenStudioEttWords(year)
        .then(arrays =>
          arrays.forEach(array => state.commit("addWords", array))
        );
    }
  }
});

export default store;
